class Income < ActiveRecord::Base
  belongs_to :category

  validates :amount, presence: true, numericality: { only_integer: true }

  validates :category, presence: true
end

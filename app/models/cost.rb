class Cost < ActiveRecord::Base
  belongs_to :category

  validates :category, presence: true

  validates :amount, presence: true, numericality: { only_integer: true }

  validate :check_total_cost

  private

  def check_total_cost
    errors.add(:amount, 'You spend too much') if (Cost.sum(:amount) + self.amount.to_i ) > Income.sum(:amount)
  end
end

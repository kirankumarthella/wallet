class HomeController < ApplicationController
	def index
		@categories = Category.all		
  end

  def statistic
    @income = Income.where("created_at > ? ", DateTime.parse(params[:month_id])).sum(:amount)
    @cost = Cost.where("created_at > ? ", DateTime.parse(params[:month_id])).sum(:amount)

    respond_to do |format|
      format.js
    end
  end

  def overview
    cost_months = Cost.all.group_by { |m| m.created_at.beginning_of_month }.keys
    income_months = Income.all.group_by { |m| m.created_at.beginning_of_month }.keys

    @arraymonth = cost_months | income_months

    # convert to hash
    @hashmonth = { }
    (0..@arraymonth.length-1).each do |i|
      @hashmonth[@arraymonth[i]] = @arraymonth[i].strftime("%Y - %m")
    end
  end
end
require 'rails_helper'

RSpec.describe Category, type: :model do
  describe 'valid' do
    it { Category.new(name: 'Test').should be_valid }
  end

  describe 'Invalid' do
    it { Category.new().should_not be_valid }
  end
end

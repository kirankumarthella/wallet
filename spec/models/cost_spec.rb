require 'rails_helper'

RSpec.describe Cost, type: :model do
  before do
    @category = Category.create(name: 'Test')
    Income.create(amount: 15, category: @category)
  end
  describe 'valid' do
    it { Cost.new(category: @category, amount: 10).should be_valid }
  end

  describe 'Invalid' do
    it 'no category' do
      expect(Cost.new(amount: 10)).not_to be_valid
    end

    it 'no amount' do
      expect(Cost.new(category: @category)).not_to be_valid
    end
  end
end

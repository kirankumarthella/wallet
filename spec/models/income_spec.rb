require 'rails_helper'

RSpec.describe Income, type: :model do
  describe 'valid' do
    before do
      category = Category.create(name: 'Test')
      @income = Income.new(category: category, amount: 10)
    end
    it { @income.should be_valid }
  end

  describe 'Invalid' do
    it 'no category' do
      expect(Income.new(amount: 10)).not_to be_valid
    end

    it 'no amount' do
      expect(Income.new(category: Category.create(name: 'test'))).not_to be_valid
    end
  end
end
